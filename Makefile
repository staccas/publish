IMAGE=staccpipes/publish
TAG=0.1.0

all: build

fmt:
	shfmt -w -s pipe.sh

build: fmt
	docker build -t publish .

push: build
	docker tag publish staccpipes/publish
	docker push "staccpipes/publish"

version:
	@echo "Using image ${IMAGE} and tag ${TAG}"
	@echo Replacing image version in pipe.yml
	sed -i "s,\(image:\).*\(# replace\),\1 ${IMAGE}:${TAG} \2,g" pipe.yml

BASE_IMAGE?=stacc.azurecr.io/cli:0.5.8
version-docker-base:
	@echo "Using base image ${BASE_IMAGE}"
	@echo Replacing base image in Dockerfile
	sed -i -n '1h;1!H;$${g;s,\(# before\n\)FROM.*\n\(# after\)\(.*\),\1FROM ${BASE_IMAGE}\n\2\3,;p;}' Dockerfile
