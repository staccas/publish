# before
FROM stacc.azurecr.io/cli:0.7.33
# after

USER root

RUN apk update && apk add --no-cache bash nodejs npm yarn

RUN addgroup -S cli && adduser -S cli -G cli
USER cli

WORKDIR /home/cli

COPY --chown=cli pipe.sh pipe.sh
RUN chmod 744 pipe.sh

ENTRYPOINT [ "/home/cli/pipe.sh" ]
